package tema5;

public class Activity {
	private String activity;
	
	public Activity(String activity){
		this.activity = activity;
	}
	
	public void setActivity(String activity){
		this.activity = activity;
	}
	
	public String getActivity(){
		return activity;
	}
	
	

}
