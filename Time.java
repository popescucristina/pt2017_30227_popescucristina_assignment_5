package tema5;

public class Time {
	private Integer year;
	private Integer month;
	private Integer day;
	private Integer hour;
	private Integer min;
	private Integer sec;
	
	public Time(Integer year, Integer month, Integer day, Integer hour, Integer min, Integer sec){
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.min = min;
		this.sec = sec;
	}
	
	public Integer getYear(){
		return year;
	}
	
	public Integer getMonth(){
		return month;
	}
	
	public Integer getDay(){
		return day;
	}
	
	public Integer getHour(){
		return hour;
	}
	
	public Integer getMin(){
		return min;
	}

	public Integer getSec(){
		return sec;
	}
}
