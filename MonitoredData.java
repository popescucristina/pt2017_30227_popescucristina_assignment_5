package tema5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class MonitoredData {
	Date startTime;
	public Date endTime;
	public String activityLabel;

	public MonitoredData(Date startTime, Date endTime, String activityLabel){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;		
	}
	
//	public static void main(String args[]) throws IOException{
//		//getAllData();
//		getData();
//	}
	public String getActivityLabel(){
		return activityLabel;
	}
	
	public Date getStartTime(){
		return startTime;
	}
	
	public Date getEndTime(){
		return endTime;
	}
	
	public void getAllData() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader("Activities.txt"));
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		    System.out.println(everything);
		} finally {
		    br.close();
		}
	}
	
	
}

