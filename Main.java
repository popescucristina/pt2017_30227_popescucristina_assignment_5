package tema5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.sound.midi.SysexMessage;
import javax.xml.crypto.Data;

public class Main {
	private static ArrayList <MonitoredData> monitoredData = new ArrayList<MonitoredData>();
	static SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd' 'hh:mm:ss");
	
	
	
	public static void main(String args[]) throws IOException, ParseException{
		getData();
		e2();
		e3();
	}
	
	public static void e3(){
		List<String> list1 = new ArrayList<String>();
		for (MonitoredData x : monitoredData) {
			list1.add(x.getActivityLabel());
		}
		
		List<Date> listData = new ArrayList<Date>();
		for (MonitoredData x : monitoredData){
			listData.add(x.getStartTime());
		}
		
		
		Object activityList;
		Map<String, Long>	activityList1 = null;// = list.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
		
		
//		Map <Map<String,Integer>, String> abc = listData.stream()
//	.collect(Collectors.groupingBy(list1.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()))), f->f.);		
	}
	
	
	
	public static void e2(){
		List<String> list = new ArrayList<String>();
		for (MonitoredData x : monitoredData) {
			list.add(x.getActivityLabel());
		}

		Map<String, Long>	activityList = list.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
		
		for (Map.Entry<String, Long> entry : activityList.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}
	}
	
	
	
	public static void getData() throws IOException, ParseException{
		BufferedReader br = new BufferedReader(new FileReader("Activities.txt"));
		
		try {
		   // StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		       
		        String[] parts = line.split("		");
		        Date sT = (Date) ft.parse(parts[0]);
		        Date eT =  (Date) ft.parse(parts[1]);
		        MonitoredData md = new MonitoredData(sT,eT, parts[2]);
		        monitoredData.add(md);
		        
		       
		        
		        
		        line = br.readLine();
		    }
		    
		    int aux = distinctDays();
		    System.out.println("distinct days"+aux);
		    
		    
		    
		} finally {
		    br.close();
		}
	}


	static int distinctDays(){
				  
		ArrayList<String> listDistinct = new ArrayList<String>();
		monitoredData.forEach((a)-> listDistinct.add(returnDay(a)));
		ArrayList<String> listFinal =  (ArrayList<String>) listDistinct.stream().distinct().collect(Collectors.toList());
	
		return listFinal.size();
	}
	
	public static String returnDay(MonitoredData md){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(md.startTime);
		//return md.startTime;
	}
	
	
	public static void ex2(){
		//Map <String, Integer> map2 = new HashMap<String, Integer>();
		//monitoredData.stream().forEach((a)-> a.distinct(a.activity).map2.add(a.activity, a.nr++));
		
//		map2 = monitoredData
//			.forEach(			
//					(a)->System.out.println(++i)
//					//.stream()
//					//.map(s->s.getActivity(a));
//					
//					)
	
		//GRESIT
//		Map <String, Integer> map = monitoredData
//										.stream()
										//.map(s->s.activityLabel)
										//.collect(Collectors.groupingBy(p->p.activityLabel));
		
		//map(s ->new Map<string, integer>{ "test", 1})
//		Map <String, Integer> map = new HashMap<String,Integer>();
//		
//		map.put("test", 11);
//		monitoredData
//				.stream()
//				.map(s-> new Map<String, Integer>{ "key" :: s.activityLabe, "Value" :: 12});
//		
//		Function<String, Long> test = x -> monitoredData.stream().count();
//		Map<String, Integer> result =
//			    monitoredData.stream().collect(Collectors.toMap(MonitoredData::getActivityLabel,
//			    		MonitoredData::getActivityCount));
//	
	
	
	}
	
	public static String getActivity(MonitoredData md){
		return md.activityLabel;
	}

}

	
